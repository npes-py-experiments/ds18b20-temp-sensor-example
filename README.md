A small example of using an DS18B20 temp sensor on RPi

# Development usage

You need Python above version 3.5, pip installed and a thingspeak.com account + channel 

1. Clone the repository `git clone git@gitlab.com:npes-py-experiments/ds18b20-temp-sensor-example.git` 
2. Create a virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment 
3. Activate the virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment
4. Install requirements `pip install -r requirements.txt`
5. On the Raspberry Pi, you will need to add dtoverlay=w1-gpio" (for regular connection) or dtoverlay=w1-gpio,pullup="y" (for parasitic connection) to your /boot/config.txt. The default data pin is GPIO4 (RaspPi connector pin 7), but that can be changed from 4 to x with dtoverlay=w1-gpio,gpiopin=x 
6. Run `python3 ds18b20_example.py` (linux) or `py ds18b20_example.py` (windows)