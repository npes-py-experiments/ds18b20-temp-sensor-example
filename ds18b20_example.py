import sys #used for unicode character \u00B0 (percentage symbol)

from w1thermsensor import W1ThermSensor #library for the sensor https://pypi.org/project/w1thermsensor/

# initialize the sensor
sensor = W1ThermSensor()

# get values from sensor in different formats
temperature_in_celsius = sensor.get_temperature()
print(f'Temperature in Celcius is: {temperature_in_celsius} degrees')

temperature_in_fahrenheit = sensor.get_temperature(W1ThermSensor.DEGREES_F)
print(f'Temperature in Fahrenheit is: {temperature_in_fahrenheit} degrees')

temperature_in_kelvin = sensor.get_temperature(W1ThermSensor.KELVIN)
print(f'Temperature in Kelvin is: {temperature_in_kelvin} degrees')

while True:

    temperature_in_all_units = sensor.get_temperatures([W1ThermSensor.DEGREES_C, W1ThermSensor.DEGREES_F, W1ThermSensor.KELVIN])
    print(f'Temperature in all units: {temperature_in_all_units[0]:.2f} \u00B0C, {temperature_in_all_units[1]:.2f} \u00B0F, {temperature_in_all_units[2]:.2f} \u00B0K')
